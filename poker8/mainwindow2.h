#ifndef MAINWINDOW2_H
#define MAINWINDOW2_H

#include <QMainWindow>
#include <QPainter>
#include <QPaintEvent>
#include <QMessageBox>
#include <QDebug>
#include "pukerclass.h"
namespace Ui {
class MainWindow2;
}

class MainWindow2 : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow2(QWidget *parent = 0);
    ~MainWindow2();
protected:
    void paintEvent(QPaintEvent *event);
public slots:
    void on_pushButton_clicked();

private:
    Ui::MainWindow2 *ui;
    void InitUI();
    void draw();
    void message();
};

#endif // MAINWINDOW2_H
