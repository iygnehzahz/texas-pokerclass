#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include<stdlib.h>
#include<time.h>
#include <QPainter>
#include <QPaintEvent>
#include <QMessageBox>
#include <QDebug>
#include "pukerclass.h"
//pukerclass vet[13][4];
namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    pukerclass **vet;
protected:
    void paintEvent(QPaintEvent *event);//实时绘制界面
public:
    void initui();
    void draw2();
    void cunchu(pukerclass **vet);
    void randomshuchu(pukerclass *con);
private:
    Ui::MainWindow *ui;

};

#endif // MAINWINDOW_H
