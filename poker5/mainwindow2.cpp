#include "mainwindow2.h"
#include "ui_mainwindow2.h"
#include "mainwindow.h"
#include<QMessageBox>
#include<QDebug>
MainWindow2::MainWindow2(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow2)
{
    ui->setupUi(this);
    InitUI();
}

MainWindow2::~MainWindow2()
{
    delete ui;
}
void MainWindow2::InitUI(){
    this->resize(1200,1200);
    this->setMaximumSize(1200,1200);
    this->setMinimumSize(1200,1200);
    this->setWindowTitle("德州puker玩法模拟");
     message();
}
void MainWindow2::paintEvent(QPaintEvent *event){
   draw();
   update();
}
void MainWindow2::draw(){
   QPainter painter(this);
   painter.drawPixmap(0,0,1600,1600,QPixmap(":/drawer/微信图片_20220506222336.jpg"));
}
void MainWindow2::message(){
QMessageBox::information(NULL,"作用介绍","本程序主要用于玩家了解掌握德州puker的基本规则，为此采用了较为简单明了的表现方式，望周知qwq",QMessageBox::Close);
}
void MainWindow2::on_pushButton_clicked(){
    MainWindow * mw=new MainWindow();
    mw->show();
    this->close();
}
